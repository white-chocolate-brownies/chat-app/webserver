module Entities where

newtype UserID        = UserID        Int deriving (Generic, Eq, Show, Ord)
newtype MessageID     = MessageID     Int deriving (Generic, Eq, Show)
newtype GroupID       = GroupID       Int deriving (Generic, Eq, Show)
newtype PrivateChatID = PrivateChatID Int deriving (Generic, Eq, Show)

newtype Year   = Year   Int deriving (Generic, Eq, Show)
newtype Month  = Month  Int deriving (Generic, Eq, Show)
newtype Day    = Day    Int deriving (Generic, Eq, Show)
newtype Hour   = Hour   Int deriving (Generic, Eq, Show)
newtype Minute = Minute Int deriving (Generic, Eq, Show)
newtype Second = Second Int deriving (Generic, Eq, Show)

data Time = Time Year Month Day Hour Minute Second
  deriving (Generic, Eq, Show)
newtype Date = Date Time
  deriving (Generic, Eq, Show)

data Timeline = Timeline
  { created   :: Date
  , destroyed :: Maybe Date
  }

data User = User
  { _userName     :: Text
  , _userID       :: UserID
  , _userTimeline :: Timeline
  }

data Message = Message
  { _messageFrom      :: User
  , _messageID        :: MessageID
  , _messageTimeline  :: Timeline
  , _messageText      :: Text
  }

data Group = Group
  { _groupName     :: Text
  , _groupID       :: GroupID
  , _groupTimeline :: Timeline
  , _groupMessages :: [Message]
  , _groupUsers    :: [User]
  }

makeLenses ''User
makeLenses ''Message
makeLenses ''Group

data PrivateChat = PrivateChat
  { from          :: User
  , to            :: User
  , privateChatID :: PrivateChatID
  , timeline      :: Timeline
  , messages      :: [Message]
  }

timeSinceCreation :: MonadIO m => Timeline -> m Time
timeSinceCreation = undefined
