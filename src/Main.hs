module Main where

import Sockets (start, State(..), Datastore(..))
import Data.Map (fromList)
import System.IO (hFlush)

main :: IO ()
main = do
  ip <- input "Enter the Server IP Address: "
  port <- safeInt (input "Enter the Server Port: ")
                  (putText "Invalid Port, try again")

  putText ""
  start tempDatastore ip port

input :: Text -> IO Text
input s = putStr s >> hFlush stdout >> getLine

safeInt :: IO Text -> IO () -> IO Int
safeInt f err = f >>= (readMaybe . unpack >>> \case
  Just x -> pure x
  Nothing -> err >> safeInt f err)

tempDatastore :: MonadIO m => Datastore m
tempDatastore = Datastore
  { save = \_ -> pure ()
  , get = pure $ State
      { _groups = []
      , _dms = []
      , _cache = fromList []
      , _datastore = undefined
      }
  }
