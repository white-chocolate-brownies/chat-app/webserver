module Prelude
  ( module Prelude
  , module Protolude
  , module Control.Lens
  , module Control.Arrow
  , module Data.Text
  , module Data.String.Interpolate
  , module Data.List
  ) where

import Protolude hiding ((<&>), (<.>), replace, Strict, from, to, uncons, unsnoc)
import Control.Lens hiding ((|>))
import Control.Arrow ((>>>))
import Data.Text (dropWhileEnd, replace, takeWhileEnd, split, dropAround, unpack)
import Data.String.Interpolate
import Data.List (partition)
import qualified Data.ByteString.Lazy (ByteString)

type LazyByteString = Data.ByteString.Lazy.ByteString

(?:) (Just x) _ = x
(?:) Nothing y  = y

(.$) f g a = f a g
(..$) f g a b = f a b g

-- This exists in a newer version of GHC
-- fold :: (Foldable t, Monoid m) => t m -> m
-- fold = foldMap identity
