module Presentation.JSON where

import Prelude hiding ((.=))
import Data.Aeson
import Entities
import Presentation.Models

instance ToJSON UserID
instance ToJSON MessageID
instance ToJSON GroupID
instance ToJSON PrivateChatID

instance FromJSON UserID
instance FromJSON MessageID
instance FromJSON GroupID
instance FromJSON PrivateChatID

instance ToJSON Year
instance ToJSON Month
instance ToJSON Day
instance ToJSON Hour
instance ToJSON Minute
instance ToJSON Second
instance ToJSON Time
instance ToJSON Date

instance ToJSON User where
  toJSON = object . userJSON

instance ToJSON Message where
  toJSON Message{..} =
    object $ ["from" .= object (userJSON _messageFrom),
              "id"   .= _messageID,
              "text" .= _messageText]
      <> timelineJSON _messageTimeline

instance ToJSON UserModel
instance ToJSON MessageModel

instance FromJSON UserModel where
  parseJSON = withObject "UserModel" $ \v -> UserModel
    <$> v .: "name"

instance FromJSON MessageModel where
  parseJSON = withObject "MessageModel" $ \v -> MessageModel
    <$> v .: "from"
    <*> v .: "text"

class (ToJSON b) => Model a b where
  toEntity :: a -> IO b

-- TODO: Update to use heterogeneous lists + monad traversals
instance Model UserModel User where
  toEntity UserModel{..} = do
    tl <- newTimeline
    id <- pure $ UserID (-1)
    pure $ User name id tl

instance Model MessageModel Message where
  toEntity MessageModel{..} = do
    tl <- newTimeline
    id <- pure $ MessageID (-1)
    user <- toEntity from
    pure $ Message user id tl text

newTimeline :: IO Timeline
newTimeline = pure $ Timeline currentTime Nothing
  where
    currentTime = Date $ Time (Year 2020) (Month 5) (Day 24)
                              (Hour 8) (Minute 29) (Second 34)

userJSON :: KeyValue a => User -> [a]
userJSON User{..} = ["name" .= _userName, "id" .= _userID]
  <> timelineJSON _userTimeline

timelineJSON :: KeyValue a => Timeline -> [a]
timelineJSON Timeline{..} = ["created" .= created]
  <> foldMap (pure . ("destroyed" .=)) destroyed
