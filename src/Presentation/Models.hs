module Presentation.Models where

data UserModel = UserModel
  { name :: Text
  } deriving Generic

data MessageModel = MessageModel
  { from :: UserModel
  , text :: Text
  } deriving Generic
