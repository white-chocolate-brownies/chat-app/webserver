{-# LANGUAGE QuasiQuotes, FlexibleContexts #-}
module Sockets where

import Presentation.JSON
import Prelude hiding (State, get, handle)
import Presentation.Models
import Entities
import Network.WebSockets hiding (send, Ping, Message)
import System.Random (randomRIO)
import Data.Aeson
import Data.Char (toLower)
import Data.Map (elems, filterWithKey)

data Client = Client Connection User

data Datastore m = Datastore
  { save :: MonadIO m => State -> m ()
  , get  :: MonadIO m => m State
  }

data State = State
  { _groups :: [Group]
  , _dms :: [PrivateChat]
  , _cache :: Map UserID Client
  , _datastore :: Datastore IO
  }

makeLenses ''State

data AuthenticationUpdate
  = Signup { email :: Text, password :: Text, name :: Text }
  | Login { email :: Text, password :: Text }
  deriving (Generic, ToJSON, FromJSON)

data Update
  = NewMessage { groupID :: GroupID, message :: MessageModel }
  | NewDirectMessage { userID :: UserID, message :: MessageModel }
  | EditMessage
    { groupID :: GroupID
    , messageID :: MessageID
    , message ::  MessageModel
    }
  deriving (Generic, ToJSON, FromJSON)

start :: Datastore IO -> Text -> Int -> IO ()
start datastore ip port = do
  state <- get datastore >>= newMVar
  putText [i|Listening on #{ip}:#{port}|]
  runServer (unpack ip) port $ application state

application :: MVar State -> PendingConnection -> IO ()
application state pending = do
  connection <- acceptRequest pending
  putText "Server: ConnectionOpened"
  forkPingThread connection 30
  handle connection (handleAuthenticate state connection) "Authenticate"

handle :: FromJSON t => Connection -> (t -> IO ()) -> Text -> IO ()
handle connection g invalidMessage = do
  message <- receiveData connection
  isPing <- checkForPing connection message

  unless isPing $ case decode message of
    Just x -> g x
    Nothing -> sendInvalidJSON connection invalidMessage

listen client@(Client connection _) state = forever $
  handle connection (handleUpdate client state) "Update"

handleAuthenticate :: MVar State -> Connection -> AuthenticationUpdate -> IO ()
handleAuthenticate state connection = \case
  Signup email password name -> do
    user@User{_userID} <- toEntity $ UserModel name
    sendJSON connection "Authenticated"
    finally (listen (Client connection user) state) (remove _userID state)
  Login {} -> sendUnsupported connection

handleUpdate :: Client -> MVar State -> Update -> IO ()
handleUpdate (Client c u) state update = do
  rState@State{..} <- readMVar state
  case update of
    NewMessage groupID model@MessageModel{text} -> do
      message <- toEntity model
      case findGroup rState groupID of
        Just group -> do
          -- TODO: Find some lens to vastly simplify this expression
--          modifyMVar__ state (groups %~ (groupMessages %~ (++ [message])))
--          modifyMVar__ state ((^. (groups . messages) %~ (++ [message])))
--          modifyMVar_ state (^. groups . (messages %~ (<> [message])))

          modifyMVar_ state
            (\s -> pure $ s { _groups = map
              (& groupMessages %~ (++ [message])) _groups })

--          modifyMVar_ state
--            (\s -> pure $ s { _groups = modifyGroup groupID
--              (\g@Group{..} -> g { _groupMessages = _groupMessages
--                <> [message] }) _groups})

          for_ (clientsOfGroup rState group) $ \(Client c _) -> send c text
        Nothing -> sendErr c "GroupID invalid"
    EditMessage {} -> sendUnsupported c
    NewDirectMessage {} -> sendUnsupported c

remove id state = pure ()

checkForPing connection message
  | message == [i|{"tag": "Ping"}|] =
      sendJSON connection "Ping Received" >> pure True
  | otherwise = pure False

modifyMVar__ s f = modifyMVar_ s (pure f)

generateID :: MonadIO m => State -> m UserID
generateID state@State{..}= do
  num <- liftIO $ UserID <$> randomRIO (1, 999999)
  if num `elem` map (\(Client _ User{..}) -> _userID) _cache
    then generateID state
    else return num

send :: Connection -> Text -> IO ()
send = sendTextData

sendJSON :: Connection -> Text -> IO ()
sendJSON c t = send c [i|{"status": "success", "message": "#{t}"}|]

sendErr :: Connection -> Text -> IO ()
sendErr c t = send c [i|{"status": "error", "message": "#{t}"}|]

sendInvalidJSON :: Connection -> Text -> IO ()
sendInvalidJSON c t = sendErr c [i|Not #{aOrAn t}#{t} json payload|]
--    a = case map toLower (head (unpack t)) of
--      Just x
--        | x `elem` "aeiou" -> "an "
--        | otherwise -> "a "
--      Nothing -> "a"

aOrAn :: Text -> Text
aOrAn = map toLower . head . unpack >>> \case
  Just x
    | x `elem` "aeiou" -> "an "
    | otherwise -> "a "
  Nothing -> "a"

sendUnsupported :: Connection -> IO ()
sendUnsupported = sendErr .$ "This operation is not supported yet"

modifyGroup :: GroupID -> (Group -> Group) -> [Group] -> [Group]
modifyGroup id f groups = map f new ++ old
  where (new, old) = partition ((== id) . _groupID) groups

findGroup :: State -> GroupID -> Maybe Group
findGroup State{..} id = find ((== id) . _groupID) _groups

clientsOfGroup :: State -> Group -> [Client]
clientsOfGroup State{..} group = group
  & _groupUsers
  & map _userID
  & (\userIDs -> filterWithKey (\uID _ -> uID `elem` userIDs) _cache)
  & elems
