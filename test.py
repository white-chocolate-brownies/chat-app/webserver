from websocket import WebSocketApp
from threading import Thread
from time import sleep

promptOn = False

def on_message(ws, msg):
    global promptOn
    promptOn = True
    print(f"< {msg}\n> ", end="")

def on_open(ws):
    def f():
        while True:
            global promptOn
            if promptOn:
                data = input()
            else:
                data = input("> ")

            promptOn = False
            ws.send(data)
            sleep(.5)
    Thread(target=f).start()

if __name__ == "__main__":
    ip = input("IP: ")
    port = int(input("port: "))

    WebSocketApp(
        f"ws://{ip}:{port}",
        on_message=on_message,
        on_open=on_open
    ).run_forever()
